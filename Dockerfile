FROM openjdk:8-slim
RUN groupadd -r spring && adduser --system --ingroup spring spring
USER spring:spring
ARG JAR_FILE=build/libs/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
HEALTHCHECK --interval=1m --timeout=3s \
  CMD curl -f http://localhost:8228/v1 || exit 1
