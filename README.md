# Springdemo

## Decisions & Results
 - Gitlab CI as the pipeline, because it's free, has great features and also open-source and can even be tested locally with gitlab-runner.
 - Gitlab as the repository, because Gitlab CI.
 - Anchore as security scanner, because it has a CIS compliance policy, and can also be extended with custom policies.
 - - My custom policy checks if the container image is based on some known distrubtion, has a fairly new openssl, and doesn't use root or docker user.
 - I didn't use deploy stage for deployment, because I wanted to provide a failing and a succeeding security scan in the same pipeline, and it's easier to see which scan results in a deployment this way.
 - Human readable reports can be downloaded as artifacts from *Scan pipeline jobs.

 ## Testing
 There are different ways to test this project:
 1. Easiest is to just take a look at previous Pipeline runs here
 1. OR request developer access for the repository and run the pipelines manually or by pushing changes
 1. OR fork the repository and change IMAGE_NAME in .gitlab-ci.yml
 1. OR get gitlab-runner for your dev machine, and run it there, but expect limited functionality
